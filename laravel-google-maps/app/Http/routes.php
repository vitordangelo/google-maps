<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/maps', function () {
    return view('maps');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/cities', 'CityController@lista');

Route::get('/marks', 'MarkController@lista');
