<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\City;

class CityController extends Controller
{
    public function lista() {
        $cidades = City::all();
        //return view('citys')->with('citys', $cidades);
        return view('maps')->with('citys', $cidades);
        //return $cidades;
    }
}
