@extends('layouts.app')

@section('content')
@if(empty($citys))
	<div class="alert alert-danger alert-position" role="alert">
		<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
	  	<span class="sr-only">Error:</span>
	  	Não há cidade cadastrada
	</div>

@else
	<h1>Listagem de cidades</h1>
	<table class="table table-striped table-bordered table-condensed text-center">
		<tr>
			<th class="text-center">ID</th>
			<th class="text-center">Nome</th>
		</tr>
		@foreach ($citys as $c)
		<tr>
			<td> {{$c->id}} </td>
			<td> {{$c->name}} </td>
		</tr>
		@endforeach
	</table>
@endif
@endsection
