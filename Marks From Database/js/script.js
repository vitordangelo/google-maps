// Mapa
function initMap() {
    //Obtém os dados externo
    $.ajax({
        url: 'data.json',
        success: function(retorno) {
             marcas = retorno;
         },
         async: false
    });
    var marcaJson = JSON.parse(marcas);

    var mapa = new google.maps.Map(document.getElementById('map'), {
        zoom: 18,
        center: {lat: -22.2550023, lng: -45.7002604},
        mapTypeId: google.maps.MapTypeId.ROAD
    });

    var grupoDeMarcas = [];
    for (var i = 0; i < marcaJson.length; i++) {
        var posicao = new google.maps.LatLng(marcaJson[i].lat, marcaJson[i].lng);
        var categoria = marcaJson[i].category;

        var marca = new google.maps.Marker({
            position: posicao,
            category: categoria,
            map: mapa
        });

        grupoDeMarcas.push(marca);
        marca.setVisible(false);
    }

    filtroCategoria = function (categoria) {
        for (var j = 0; j < marcaJson.length; j++) {
            marca = grupoDeMarcas[j];
            if (marcaJson[j].category == categoria || marcaJson[j].category.length === 0) {
                marca.setVisible(true);
            }
            else {
                marca.setVisible(false);
            }
        }
    }

};
initMap();
